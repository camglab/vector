# Vector

## example

测试前先设置环境变量：

```
# 项目根目录，需要导入vector和htmlplot
export PYTHONPATH=$PWD

```


## vector.portfolio.Portfolio

账户回测基类

### 需要继承实现的方法

#### init

```python 
def init(self, symbols: list)
```

回测开始前初始化，可以不实现，如果继承需要调用super()

|params|type|description|
|:-:|:-:|:-:|
|symbols|list|需要回测的品种|

#### algorithm

```python
def algorithm(self, data: pd.Series)
```

进出场算法写在这里，传入参数为当前时间节点的数据。

|params|type|description|
|:-:|:-:|:-:|
|data|pd.Series(index=pd.MultiIndex)|数据，包含每个symbol的k线和自定义数据|

```
data:
symbol  field 
bch     close       379.530000
        cross         0.000000
        fast        383.164000
        high        381.650000
        low         370.690000
        open        370.850000
        slow        406.438333
        volume    12860.156350
btc     close     30740.330000
        cross         0.000000
        fast      30444.007000
        high      30850.000000
        low       29712.940000
        open      29719.930000
        slow      31413.466000
        volume     5356.034436
eth     close      1268.770000
        cross         0.000000
        fast       1255.101000
        high       1279.290000
        low        1224.020000
        open       1225.550000
        slow       1292.958000
        volume    99028.642530
Name: 2021-01-28 03:00:00, dtype: float64
```

#### on_order

```python
def on_order(self, order: Order)
```

|params|type|description|
|:-:|:-:|:-:|
|order|portfolio.Order|Order对象|


Order结构体：

```python
@dataclass

class OrderStatus(Enum):
    
    Pending = 0
    Holding = 1
    Finished = 2
    Canceled = 3

class Order:
    
    orderId: str = ""
    symbol: str = ""
    volume: float = 0
    entryDt: datetime = None
    entryPrice: float = 0
    entryVolume : float = 0
    entryType: str = "default"
    exitDt: datetime = None
    exitPrice: float = 0
    exitVolume: float = 0
    exitType: str = "default"
    commission: float = 0
    status: OrderStatus = OrderStatus.Pending
```

### 运行

#### backtest

```python
def backtest(self, data: pd.DataFrame, symbols: list): => balance: pd.DataFrame
```

|params|type|description|
|:-:|:-:|:-:|
|data|pd.DataFrame(index=pd.Index(), columns=pd.MultiIndex(names=["symbol", "field"]))|回测数据|
|symbols|list|需要回测的品种|
|balance|pd.DataFrame|历史持仓|


data格式

```
symbol                    btc                                           
field                    open      high       low     close       volume
trade_date                                                              
2021-01-28 08:00:00  30362.19  30728.57  29842.10  30671.86  3572.830400
2021-01-28 09:00:00  30676.28  30917.53  30370.79  30881.94  2442.242383
2021-01-28 10:00:00  30880.57  31214.51  30788.30  30981.72  2922.950993
2021-01-28 11:00:00  30987.85  31526.78  30902.81  31404.25  4471.713914
2021-01-28 12:00:00  31406.90  31590.00  31297.87  31496.68   511.867452
```

balance格式

```
symbol               btc        eth        bch      
position            long short long short long short
trade_date                                          
2020-01-01 00:00:00    0     0    0     0    0     0
2020-01-01 01:00:00    0     0    0     0    0     0
2020-01-01 02:00:00    0     0    0     0    0     0
2020-01-01 03:00:00    0     0    0     0    0     0
2020-01-01 04:00:00    0     0    0     0    0     0
...                  ...   ...  ...   ...  ...   ...
2020-01-31 19:00:00    0    -1    0     0    0    -1
2020-01-31 20:00:00    0    -1    0     0    0    -1
2020-01-31 21:00:00    0    -1    0     0    0     0
2020-01-31 22:00:00    0    -1    0     0    0     0
2020-01-31 23:00:00    0    -1    0     0    0     0
```

### 回测中可以调用的方法

#### entry_order

```python
def entry_order(self, symbol: str, volume: float, price: float = 0) -> order_id: str 
```

|params|type|description|
|:-:|:-:|:-:|
|symbol|str|品种|
|volume|float|单量，正负表示多空|
|price|float|限价，0表示市价，默认0|
|order_id|str|生成订单的订单号|

#### cancel_entry

```python
def cancel_entry(self, order_id: str) -> result: bool
```

|params|type|description|
|:-:|:-:|:-:|
|order_id|bool|订单号|
|result|bool|撤单是否成功，失败说明订单已成交|

#### exit_order

```python
def exit_order(self, order: Order, volume: float = 0, price: float = 0)
```

|params|type|description|
|:-:|:-:|:-:|
|symbol|str|品种|
|volume|float|单量，符号与进场时的符号一致，0表示全出，默认0|
|price|float|限价，0表示市价，默认0|

#### cancel_exit

```python
def cancel_exit(self, order_id: str) -> result: bool
```

|params|type|description|
|:-:|:-:|:-:|
|order_id|str|订单号|
|result|bool|撤单是否成功，失败表示改单没有正在出场的挂单|

#### is_order_finished

```python
def is_order_finished(self, order_id: str) -> result: bool
```

|params|type|description|
|:-:|:-:|:-:|
|order_id|str|订单号|
|result|bool|订单是否已完成|

#### get_holding_order

```python
def get_holding_order(self, order_id: str) -> order: Order
```

找到未结束的订单对象，订单不存在会抛出KeyError

|params|type|description|
|:-:|:-:|:-:|
|order_id|str|订单号|
|order|Order|订单对象|

#### set_autoexit

```python
def set_autoexit(self, order_id: str, stoploss: float = 0, takeprofit: float = 0)
```

|params|type|description|
|:-:|:-:|:-:|
|order_id|str|订单号|
|stoploss|float|止损价格|
|takeprofit|float|止盈价格|

#### set_trailing_stop

```python
def set_trailing_stop(self, order_id: str, trailing_percentage: float, trailing_price: float)
```

追踪止损

|params|type|description|
|:-:|:-:|:-:|
|order_id|str|订单号|
|trailing_percentage|float|止损百分比，小数，0.01表示最大回撤1%即止损|
|trailing_price|float|初始追踪价格，一般为下单价格，在持仓过程中如果标的价格高于该价格会更新|


#### timestop

```python
def timestop(self, order_id: str, expire_at: datetime)
```

设定出场时间，到达时间后出场。

|params|type|description|
|:-:|:-:|:-:|
|order_id|str|订单号|
|expire_at|datetime|出场时间|


### 绩效展示

绩效计算后存储在Portfolio.backtest_results中：

```python
Portfolio.backtest_results = {
        "order_performance": { # 订单收益表
                "all": pd.DataFrame,
                "symbol1": pd.DataFrame,
                ...
        },
        "order_performance_statistics": { # 订单收益统计
                "all": dict,
                "symbol1": dict,
                ...
        },
        "positions": pd.DataFrame, # 历史仓位(分品种和多空)
        "holding": pd.DataFrame, # 历史市值(分品种和多空)
        "trades": pd.DataFrame, # 历史成交表
        "orders": pd.DataFrame, # 历史订单表

}
```

#### 订单绩效

```python
def cal_order_performance(self, symbols: list=None, contracts: dict=None)
```

计算订单绩效

|params|type|description|
|:-:|:-:|:-:|
|symbols|list|品种，默认为所有品种绩效合并计算|
|contracts|dict|品种交易信息：{"symbol": {"rate": 0, "slippage": 0}}|

获取绩效：

```python
Portfolio.backtest_results["order_performance"]["symbol"|"all"]
Portfolio.backtest_results["order_performance_statistics"]["symbol"|"all"]
```


#### 持仓绩效

```python
def cal_period_performance(self, bars: pd.DataFrame, contracts: dict = None)
```

params|type|description|
|:-:|:-:|:-:|
|bars|pd.DataFrame|回测数据|
|contracts|dict|品种交易信息：{"symbol": {"rate": 0, "slippage": 0}}|


```python
def get_period_statistics(self, symbols: Union[List[str], None] = None, init_cash: float = 0, periods_per_year: int = 365, freq: str = "") -> balance: pd.DataFrame, statistics: dict
```

获取前先调用`cal_period_performance`计算持仓和成交

|params|type|description|
|:-:|:-:|:-:|
|symbols|list|品种，默认为所有品种绩效合并计算|
|init_cash|float|初始金额|
|periods_per_year|int|波动率年化乘数|
|freq|str|周期, "d\|h\|m"|
|balance|pd.DataFrame|账户收益走势|
|statistics|dict|账户收益统计|

---

# data_source

`data_source`用于回测前的数据处理。

## `data_source.SourceManager`

`data_source.SourceManager`用于从数据库拉取和更新数据到本地，并整理成不同周期存储。


### 构造方法

```python
classmethod
def from_mongodb(cls, host: str, db: str, root="~/.vector_data", default_freq="1min") -> SourceManager:
```

类方法用于构造SourceManager对象。

|params|type|description|
|:-:|:-:|:-:|
|host|str|数据库地址|
|db|str|数据库名|
|root|str|本地缓存地址，默认`~/.vector_data`|
|default_freq|str|默认k线周期1min|


### 需要继承的继承方法

```python
def source_key_map(self, key: str) -> str:
```

品种名到数据源的映射，例如需要"eth"的数据，但是eth在数据库中的表名是"eth_spot:binance"，则需要将"key"映射为"{key}_spot:binance"


```python
def target_key_map(self, key: str) -> str:
```

品种名到本地缓存文件名的映射


### 拉取数据到本地

```python
def pull(self, keys: List[str], begin: int = 0, end: int = 0):
```

|params|type|description|
|:-:|:-:|:-:|
|keys|List[str]|需要拉取品种|
|begin|int|开始时间(timestamp)，一般需要指定开始时间，如果本地已有数据，开始时间为0不会有影响，如果本地没有数据，开始时间为0则不会导入数据|
|end|int|结束时间(timestamp)，结束时间为0会将已有的数据向后更新到最新|


### 合成不同周期k线

```python
def resample(self, keys: List[str] , freqs: List[str], begin: int = 0, end: int = 0):
```

根据本地缓存的一分钟k线合成不同周期k线并缓存。

|params|type|description|
|:-:|:-:|:-:|
|keys|List[str]|需要拉取品种|
|freqs|List[str]|周期：30min, 1h, 1d|
|begin|int|开始时间(timestamp)，开始时间为0将已有的数据向前更新到最新|
|end|int|结束时间(timestamp)，结束时间为0会将已有的数据向后更新到最新|


### 读取数据

```python
def load(self, keys: List[str], freqs: List[str], begin: int=0, end: int=0, columns: List[str]=None, closed_interval=(True, False), tzinfo=UTC) -> Dict[str, DataFrame]:
```

从缓存读取数据。


|params|type|description|
|:-:|:-:|:-:|
|keys|List[str]|需要拉取品种|
|freqs|List[str]|周期：30min, 1h, 1d|
|begin|int|开始时间(timestamp)|
|end|int|结束时间(timestamp)，结束时间为0表示到最新的数据|
|columns|List[str]|需要的列，默认为价格和成交量|
|closed_interval|Tuple[bool, bool]|开始和结束时间点是否包含，默认为左闭右开(True, False)|
|tzinfo|timezone|数据转化为datetime对象的失去，默认为UTC|
|result|Dict[str, DataFrame]|按不同周期合并的DataFrame|


## `data_source.DataManager`

用于数据处理的框架，提供了对单品种的处理，多品种的处理和不同周期数据的自动合并。

### 构造方法


```python
def __init__(self, datas: Dict[str, pd.DataFrame] = None) -> None:
```

|params|type|description|
|:-:|:-:|:-:|
|datas|Dict[str, DataFrame]|按不同周期合并的DataFrame|


```python
def add(self, datas: Dict[str, pd.DataFrame]):
```

加入新数据

|params|type|description|
|:-:|:-:|:-:|
|datas|Dict[str, DataFrame]|按不同周期合并的DataFrame|


### 需要继承实现的方法

```python
def handle_symbol(self, symbol: str, freq: str, data: pd.DataFrame) -> pd.DataFrame:
```

接受但品种的数据并返回对该品种的处理结果，返回的数据index需要与传入数据对齐。


```python
def handle_all(self, freq: str, data: pd.DataFrame) -> pd.DataFrame:
```

接受多品种的数据并返回对该品种的处理结果，返回的数据index需要与传入数据对齐，columns需要是MultiIndex且第一行为symbol。


### 执行算法生成数据

```python
def prepare_data(self):
```

会执行上面继承后的算法并将算出来的数据合并到最小周期的一张表:`DataManager.basic_data`,该表可以用于计算portfolio进行计算。









