from vector import portfolio
import pandas as pd
from datetime import datetime, timedelta



class Trader(portfolio.Portfolio):
    
    def __init__(self):
        super().__init__()
        self.order_ids = {}
        self.symbols = []
        
    def init(self, symbols: list):
        super().init(symbols)
        for symbol in symbols:
            self.order_ids[symbol] = ""
        self.symbols = symbols
    
    
#     def algorithm(self, s: pd.Series):
#         """long"""
#         cross = s[:, "cross"]
#         for symbol, signal in cross.items():
#             if signal == 1:
#                 if not self.order_ids[symbol]:
#                     order_id = self.entry_order(symbol, 1)
#                     self.order_ids[symbol] = order_id
#                     self.set_trailing_stop(order_id, 0.015, s[symbol]["high"])
                    
#             elif signal == -1:
#                 orderid = self.order_ids[symbol]
#                 if orderid:
#                     order = self.get_holding_order(orderid)
#                     self.exit_order(order)
                    
    def algorithm(self, s: dict):
        """short"""
        for symbol in self.symbols:
        
            signal = s[symbol]["cross"]
        # cross = s[:, "cross"]
        # for symbol, signal in cross.items():
            if signal == -1:
                if not self.order_ids[symbol]:
                    order_id = self.entry_order(symbol, -1)
                    self.order_ids[symbol] = order_id
                    self.set_trailing_stop(order_id, 0.015, s[symbol]["low"])
                    
            elif signal == 1:
                orderid = self.order_ids[symbol]
                if orderid:
                    order = self.get_holding_order(orderid)
                    self.exit_order(order)
    
    def on_order(self, order: portfolio.Order):
        if order.status == portfolio.OrderStatus.Holding:
            self.timestop(order.orderId, order.entryDt + timedelta(hours=12))
        elif order.status == portfolio.OrderStatus.Finished:
            self.order_ids[order.symbol] = ""