from vector import data_source
from vector.portfolio import *
import importlib
import pandas as pd
import tables as tb
from datetime import datetime, timedelta, timezone
from itertools import accumulate, chain
import logging


MONGODB_HOST = "172.16.11.81"
KLINE_DB = "Kline_1Min_Auto_Db_Plus"
symbols = ["btc", "eth"]
freqs = ["5min", "1h"]
utc = timezone(timedelta())


class Trader(Portfolio):
    """测试案例，用于测试混合止损止盈。

    :param Portfolio: [description]
    :type Portfolio: [type]
    """
    
    def __init__(self):
        super().__init__()
        self.order_ids = set()
        self.symbols = []
        self.init_cash = 100000
        
    def init(self, symbols: list):
        super().init(symbols)
        self.symbols = symbols

    def algorithm(self, data: dict):
        if not self.order_ids:
            """没有持仓时开仓并记录订单号
            """
            for symbol in self.symbols:
                order_id = self.entry_order(
                    symbol,
                    round(self.init_cash/data[symbol]["close"], 2)
                )
                self.order_ids.add(order_id)
        else:
            """有开仓时对所有的订单做混合止盈止损，幅度都为5%。
            """
            self.mix_autoexit(data, list(self.order_ids), 0.05, 0.05)

    def on_order(self, order: Order):
        if order.status == OrderStatus.Finished:
            self.order_ids.discard(order.orderId)


def cal_pair_profit(orders: pd.DataFrame):
    """计算订单的综合收益

    :param orders: 订单表
    :type orders: pd.DataFrame
    :return: 综合收益 = 订单总收益 / 订单总成本
    :rtype: float
    """
    return ((orders["exitPrice"] - orders["entryPrice"]) * orders["exitVolume"]).sum() \
        / (orders["entryPrice"] * orders["exitVolume"].abs()).sum()


if __name__ == "__main__":

    # 直接用本地数据源
    ds = data_source.SourceManager.local()
    freq = "1min"

    # 用1分钟数据
    result = ds.load(
        ["btc", "eth"], [freq], 
        int(datetime(2020, 1, 1).timestamp()), 
        int(datetime(2020, 3, 1).timestamp())
        )

    data = result[freq]

    trader = Trader()
    trader.backtest(data, ["eth", "btc"])

    orders = trader.history_orders()

    trade_profit = orders.groupby(orders["exitDt"]).apply(cal_pair_profit)
    print(trade_profit)
    """总体利润率在0.05或-0.05左右
    >>> exitDt
        2020-01-05 16:31:00+00:00    0.050766
        2020-01-07 00:16:00+00:00    0.051732
        2020-01-14 03:56:00+00:00    0.051447
        2020-01-14 17:02:00+00:00    0.051185
        2020-01-17 08:14:00+00:00    0.053164
        2020-01-19 12:26:00+00:00   -0.051597
        2020-01-28 00:39:00+00:00    0.051541
        2020-01-30 18:11:00+00:00    0.050015
        2020-02-05 15:54:00+00:00    0.050025
        2020-02-06 12:13:00+00:00    0.050321
        2020-02-09 03:13:00+00:00    0.050714
        2020-02-12 00:47:00+00:00    0.055510
        2020-02-12 20:48:00+00:00    0.050004
        2020-02-16 15:37:00+00:00   -0.050331
        2020-02-16 16:53:00+00:00   -0.050106
        2020-02-16 23:09:00+00:00    0.050799
        2020-02-17 14:12:00+00:00   -0.050015
        2020-02-17 23:24:00+00:00    0.050234
        2020-02-18 18:05:00+00:00    0.050383
        2020-02-19 21:48:00+00:00   -0.054756
        2020-02-23 06:16:00+00:00    0.050900
        2020-02-25 14:27:00+00:00   -0.050409
        2020-02-26 02:07:00+00:00   -0.050191
        2020-02-26 16:17:00+00:00   -0.051135
        2020-02-29 15:59:00+00:00   -0.002941
        dtype: float64
    """


